const fs = require('fs');
const moment = require('moment');

const imagesPath = '/opt/node/nwz/service/public/images/media/';

fs.readdir(imagesPath, (err, files) => {
	files.forEach(file => {
		const stats = fs.statSync(`${imagesPath}${file}`);
		const diff = moment(new Date()).diff(stats.birthtime, 'days');
		if (diff > 1) {
			fs.unlinkSync(`${imagesPath}${file}`);		
		}
	});
});