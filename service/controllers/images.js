var http = require('http');
var https = require('https');
var fs = require('fs');

module.exports = {
    saveImage: function(url, name) {
        return new Promise(function(resolve, reject) {
            var request = http.get(url, function(res) {
                var imagedata = ''
                res.setEncoding('binary')

                res.on('data', function(chunk) {
                    imagedata += chunk
                })

                res.on('end', function() {
                    var webPath = "/images/media/" + name + "." + url.split(".")[url.split(".").length - 1];
                    var path = __dirname + "/../public" + webPath;
                    fs.writeFile(path, imagedata, 'binary', function(err) {
                        if (err) throw err
                        resolve(webPath);
                    })
                })

                res.on('error', reject)

            });
        });
    },
    getImage: function(url) {
        return new Promise(function(resolve, reject) {
            var protocol = url.match("https") ? https : http;
            var request = protocol.get(url, function(res) {
                var imagedata = ''
                res.setEncoding('binary')

                res.on('data', function(chunk) {
                    imagedata += chunk
                })
                res.on('end', function() {
                    resolve(imagedata);
                })

                res.on('error', reject)

            });
        }).catch(function(err){
            console.log(err);
        });
    },
}

//saveImage("http://d2vpb0i3hb2k8a.cloudfront.net/wp-content/uploads/sites/7/2017/08/01/vidal-ps-2-320x190.jpg", "pepe")