var express = require('express');
var router = express.Router();
var idToString = function(row) {
        for (var i = 0; i < row.length; i++) {
            row[i]._id = row[i]._id.toString();
        }
        return row;
    }

router.get('/:url', function (req, res, next) {
  var url = req.params.url;
  var noticia = require("../models/_noticia");
  noticia.find(url, function (articulo) {
    articulo = idToString(articulo);
    res.render('vistaNoticia', {
      noticia: articulo[0]
    });
  });
});


/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
