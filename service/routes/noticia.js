var express = require('express');
var router = express.Router();
var noticia = require('../models/_noticia');
var images = require("../controllers/images");
var routerNoticia = {
    idToString: function(row) {
        for (var i = 0; i < row.length; i++) {
            row[i]._id = row[i]._id.toString();
        }
        return row;
    },
    toList: function(req, res) {
        res.redirect('noticia/list');
    },
    list: function(req, res) {
        try {
            var skip = (req.query.skip ? parseInt(req.query.skip) : 0);
            noticia.list(skip, function(row) {
                res.send({
                    noticia: row
                });
            });
        } catch (ex) {}
    },
    classifyList: function(req, res) {
        try {
            //This method get 1000 records classified to use as base.
            noticia.classifyList(function(row) {
                res.send({
                    noticia: row
                });
            });
        } catch (ex) {}
    },
    getMedia: function(req, res) {
        var url_img = req.query.media;
        images.getImage(url_img)
            .then(function(data) {
                res.end(data, 'binary');
            });
    },
    update: function(req, res) {
        var titulo_noticia = req.body.titulo_noticia;
        req.body.media_noticia = "/noticia/media/?media=" + req.body.media_noticia;
        noticia.find(titulo_noticia, function(noticias) {
            if (noticias.length === 0) {
                noticia.insert(req.body, function(nuevaNoticia) {
                    res.send({
                        data: nuevaNoticia
                    });
                });
            } else {
                noticia.update(req.body, titulo_noticia, function() {});
            }
        });
    },
    get: function(req, res) {
        var url = req.params.url.split(".")[0];
        var _format = req.params.url.split(".")[1];
        noticia.find(url, function(articulo) {
            articulo = routerNoticia.idToString(articulo);
            if (_format !== "json") {
                res.render('vistaNoticia', {
                    noticia: articulo[0]
                });
            } else {
                res.send({
                    data: articulo
                });
            }
        });
    }
};


router.get('/', routerNoticia.toList);
router.get('/media', routerNoticia.getMedia);
router.get('/list', routerNoticia.list);
router.get('/list.:format/', routerNoticia.list);
router.post('/new', routerNoticia.update);
router.get('/:url', routerNoticia.get);
module.exports = router;