//initial data insert
var fs = require("fs");
var conn = require("../connection");
var _ = require("lodash");
var collection = "";
//Recorremos nuestro directorio
var dirname = "./initial_data/";
var fileNumber = 0;
fs.readdir(dirname, function(err, filenames) {
    console.log("Entro");
    if (err) {
        console.error(err);
        return;
    }
    filenames.forEach(function(filename) {
        fileNumber++;
        var rx = new RegExp(/.*\.json$/);
        if (!rx.test(filename)) {
            return;
        }
        
            console.log(filename);
            fs.readFile(dirname + filename, "utf-8", function(err, content) {
                if (err) {
                    console.error(err);
                    return;
                }
                console.log(filename)
                bulkInsert(filename, content);
            });
        
    });
});
function bulkInsert(filename, content) {
    try{
        var collectionName = collection || filename.replace(".json", "");
        var collectionObj = require("../models/_" + collectionName);
        var data = JSON.parse(content);
        _.forEach(data, function(item) {
            console.log(item);
            collectionObj.insert(item, function( docs, err) {
                if (err) {
                    console.error(JSON.stringify(err));
                }else {
                    console.log(docs);
                }
            });
        });
    }catch(ex){
        console.error(ex);
    }
}
