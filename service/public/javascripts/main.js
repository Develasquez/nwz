var newsUrl = (document.location.href.match(/asdasd/) ? "https://nwz.cl" : "") + "/noticia/list.json";
var itemsConfig = [0, 1, 1, 2, 2, 1, 1, 0, 2, 2, 2, 2, 0, 1, 1, 2, 2, 1, 1, 0, 0];
var main = {
    init: function () {
        main.getNews().then(main.addNews);
    },
    getNews: function () {
        return new Promise(function (resolve, reject) {
            fetch(newsUrl).then(function (response) {
                if (response.status === 200) {
                    response.json().then(function (data) {
                        resolve(data);
                    });
                } else {
                    //from DB;
                    reject();
                }
            });
        });
    },
    getHumanTime: function (time) {
        var date1 = new Date();
        var date2 = new Date(time);
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffSecs = Math.ceil(timeDiff);
        return humanizeDuration(diffSecs).split("-")[0];
    },
    getFirstWords: function (body) {
        var words = "";
        $(body).each(function (index, element) {

            words += $(element).is("p") ? $(element).text() : '';
        });
        return words.substring(0, 144);
    },
    addNews: function (data) {
        var odd, even = null;
        $.each(data.noticia, function (index, element) {
            element.bajada_noticia = (element.bajada_noticia.length > 0 ? element.bajada_noticia.substring(0, 144) : main.getFirstWords(element.cuerpo_noticia)) + "...";
            var card;
            var isFirstVertical = itemsConfig[index] === 2 && itemsConfig[index + 1] === 2;
            if (isFirstVertical && !odd) {
                odd = $(main.classStyles[itemsConfig[index]](element)).attr('index', index);
                card = '<div class="aspect_vertical index_' + index + '">';
            }
            else if (odd && itemsConfig[index] === 2) {
                even = main.classStyles[itemsConfig[index]](element);
                card = null;
                $(".index_" + (index - 1)).append(odd).append(even);
                odd = null;
            }
            else {
                card = main.classStyles[itemsConfig[index]](element);
            }

            $(".today").append($(card).data("article",element));
        });
    },
    classStyles: [function (article) {
        return [
            '<a ref="' + article._id + '" class="noticia aspect_50_50">',
            '   <div class="card z-d1  white" style="background-image: url(\'' + article.media_noticia + '\')">',
            '    <div><span class="color-pink"><i class="icon-clock-o"></i> ' + main.getHumanTime(article.fecha_creacion) + '</span></div>',
            '    <div><h2>' + article.titulo_noticia + '</h2></div>',
            '   </div>',
            '</a>'
        ].join('\n');
    }, function (article) {
        return [
            '<a ref="' + article._id + '" class="noticia aspect_50_25" >',
            '   <div class="card z-d1 white single">',
            '    <img class="media" src="' + article.media_noticia + '">',
            '       <div class="header">',
            '           <span class="color-pink"><i class="icon-clock-o"></i> ' + main.getHumanTime(article.fecha_creacion) + '</span>',
            '           <h2>' + article.titulo_noticia + '</h2>',
            '       </div>',
            '       <div class="text">',
            '           <p>' + article.bajada_noticia + '</p>',
            '       </div>',
            '   </div>',
            '</a>'
        ].join('\n');
    }, function (article) {
        return [
            '<a ref="/' + article._id + '" class="noticia aspect_25_50" >',
            '   <div class="card z-d1 white single">',
            '       <img class="media" src="' + article.media_noticia  + '">',
            '       <div class="body">',
            '           <span class="color-pink"><i class="icon-clock-o"></i> ' + main.getHumanTime(article.fecha_creacion) + '</span>',
            '           <h2>' + article.titulo_noticia + '</h2>',
            '           <p>' + article.bajada_noticia + '</p>',
            '       </div>',
            '   </div>',
            '</a>'
        ].join('\n');
    }]
};





$(main.init);