var CACHE_NAME = '0.0.2'; //Cache Name

//Files to cache
var filesToCache = [
  '/',
  '/manifest.json',
  '/stylesheets/fonts/fontawesome-webfont.eot',
  '/stylesheets/fonts/fontawesome-webfont.svg',
  '/stylesheets/fonts/fontawesome-webfont.ttf',
  '/stylesheets/fonts/fontawesome-webfont.woff',
  '/stylesheets/fonts/fontawesome-webfont.woff2',
  '/stylesheets/fonts/FontAwesome.otf',
  '/stylesheets/fonts/Roboto-Regular.ttf',
  '/stylesheets/monomer.css?v=0.0.2',
  '/stylesheets/style.css?v=0.0.2',
  '/javascripts/vendor/jquery.js?v=0.0.2',
  '/javascripts/vendor/pointerevents.js?v=0.0.2',
  '/javascripts/vendor/monomer.js?v=0.0.2', 
  '/javascripts/vendor/humanize-duration.js?v=0.0.2'
];




//Adding 'install' event listener
self.addEventListener('install', function (event) {
  // Perform install steps
  event.waitUntil(
    caches.open(CACHE_NAME)
      .then(function (cache) {

        return cache.addAll(filesToCache);
      })
      .catch(function (err) {
        console.log("Error occurred while caching ", err);
      })
  );
});

//Adding 'activate' event listener
self.addEventListener('activate', function (event) {
  console.log('Event: Activate');
});

//Adding 'fetch' event listener

self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.match(event.request)
      .then(function (response) {
        // Cache hit - return response
        if (response) {
          return response;
        }
        return fetch(event.request);
      }
      )
  );
});


// triggered everytime, when a push notification is received.
self.addEventListener('push', function (event) {

  console.info('Event: Push');

  var title = 'New commit on Github Repo: RIL';

  var body = {
    'body': 'Click to see the latest commit',
    'tag': 'pwa',
    'icon': './images/48x48.png'
  };

  event.waitUntil(
    self.registration.showNotification(title, body)
  );
});


self.addEventListener('notificationclick', function (event) {

  var url = './latest.html';

  event.notification.close(); //Close the notification

  // Open the app and navigate to latest.html after clicking the notification
  event.waitUntil(
    clients.openWindow(url)
  );

});