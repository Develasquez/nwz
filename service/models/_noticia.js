var connection = require('../connection');
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var mySelf = {
  getMongoObj: function() {
    return {
      referencia: [{
        type: Schema.Types.ObjectId,
        ref: 'referencia'
      }],
      url_noticia: String,
      url_noticia_referencia: String,
      titulo_noticia: String,
      bajada_noticia: String,
      media_noticia: String,
      media_base64: String,
      tipo_media_noticia: String,
      cuerpo_noticia: String,
      cantidad_accesos: Number,
      fecha_creacion: Date,
      tiempo_permanencia_promedio: String,
      categoria: [{
        type: Schema.Types.ObjectId,
        ref: 'categoria'
      }]

    };
  },
  insert: function(params, _function) {
    var schema = mySelf.getMongoObj();
    connection.open('noticia', schema, function(collection, error) {
      if (error) {
        _function(undefined, error);
        return false;
      }
      collection.create(params, function(err, docs) {
        _function(docs, err);
      });
    });
  },
  increaseAccess: function(titulo_noticia, _function) {
    var schema = mySelf.getMongoObj();
    connection.open('noticia', schema, function(collection) {
      var query = collection.update({
        titulo_noticia: titulo_noticia
      }, {
        $inc: {
          cantidad_accesos: 1
        }
      });
      query.exec(function(err, docs) {
        _function(docs, err);
      });
    });
  },
  find: function(url, _function) {
    var schema = mySelf.getMongoObj();
    connection.open('noticia', schema, function(collection) {
      var query = collection.find({
        url_noticia: url
      });
      query.exec(function(err, docs) {
        _function(docs, err);
      });
    });
  },
  list: function(skip, _function) {
    var schema = mySelf.getMongoObj();
    connection.open('noticia', schema, function(collection) {
      if (typeof skip === "function") {
        _function = skip;
        skip = 0;
      }
      collection.find({
          "url_noticia": {
            '$ne': 'noticia'
          }
        })
        .limit(20)
        .skip(skip)
        .sort({
          "fecha_creacion": -1,
          "cantidad_accesos": -1
        })
        .populate('referencia')
        .exec(function(err, docs) {
          _function(docs, err);
        });
    });

  },
  classifyListlist: function(_function) {
    var schema = mySelf.getMongoObj();
    connection.open('noticia', schema, function(collection) {
      collection.find({
          "url_noticia": {
            '$ne': 'noticia'
          }
        })
        .limit(20)
        .skip(skip)
        .sort({
          "fecha_creacion": -1,
          "cantidad_accesos": -1
        })
        .populate('referencia')
        .exec(function(err, docs) {
          _function(docs, err);
        });
    });

  },
  update: function(titulo_noticia, params, _function) {
    var schema = mySelf.getMongoObj();
    var whereObject = {
      titulo_noticia: titulo_noticia
    };

    connection.open('noticia', schema, function(collection) {
      collection.update(whereObject, {
        $set: params
      }, {
        multi: true
      }, function(err, docs) {
        _function(docs, err);
      });
    });
  }
};
module.exports = mySelf;